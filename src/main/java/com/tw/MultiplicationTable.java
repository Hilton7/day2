package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        if (isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end)) {
            return true;
        }
        return false;
    }

    public Boolean isInRange(int number) {
        if (number >= 1 && number <= 1000) {
            return true;
        }
        return false;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        if (start > end) {
            return false;
        }
        return true;
    }

    public String generateTable(int start, int end) {
        StringBuilder table = new StringBuilder("");
        for (int i = start; i <= end; i++) {
            table.append(generateLine(start, i));
            if (i != end) {
                table.append(String.format("%n"));
            }
        }
        return table.toString();
    }

    public String generateLine(int start, int row) {
        StringBuilder line = new StringBuilder("");
        for (int i = start; i <= row; i++) {

            line.append(generateSingleExpression(i, row));
            if (i != row) {
                line.append("  ");
            }
        }
        return line.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand * multiplier;
    }
}
